console.log("Töötan!")

// 1. Ülesanne: Alla laadida API/st teksti.

    var refreshMessages = async function() {
	// Lihtsalt selleks, et ma tean, et funktsioon läks käima.
	console.log("refreshMessages läks käima")

	//uus muutuja kuhu salvestame praeguse toa
	var tuba = document.querySelector("#room").value

	// API aadress on string, salvestan lihtsalt muutujasse.
	var APIurl = "http://localhost:8080/chat/" + tuba
	// fetch teeb päringu serverisse (meie defineeritud adre)
	var request = await fetch(APIurl) // await - oota ära vastust
	// json() käsk vormindab meile data mugavaks JSONiks
	var sonumid = await request.json()
	console.log(sonumid);

	// async on otseselt seotud await-ga

	// document.querySelector('#jutt').innerHTML = JSON.stringify(json)

	// Ülesanne - Kuva serverist saadud info HTMLis (ehk lehel)

	document.querySelector('#jutt').innerHTML = ""

	while (sonumid.length > 0) { // kuniks sõnumeid on
		var sonum = sonumid.shift() // pop või shift - viimane või esimene

		// lisa HTMLi #jutt sisse sonum.message
	document.querySelector('#jutt').innerHTML += `<p><img src= '${sonum.url}' width="100">${sonum.username}: ${sonum.message}</p>`

	console.log(sonum)
	}

	window.scrollTo(0,document.body.scrollHeight);
    }


    setInterval(refreshMessages, 1000) // 1000 on sekund

    document.querySelector('form').onsubmit = function(event) {
	event.preventDefault()

	// console.log("submit käivitus")

	// Ülesanne: Korjame kokku formist info
	var username = document.querySelector('#username').value
	var url = document.querySelector('#url').value
	var message = document.querySelector('#message').value
	document.querySelector('#message').value = "" // tee imput tühjaks
	console.log(username, url, message)

	// POST päring postitab uue andmetüki serverisse
	var tuba = document.querySelector("#room").value
	var APIurl = "http://localhost:8080/chat/"+tuba+"/new-message" // See on serveri poolt antud URL
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({username: username, message: message, url: url}),
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}
	    )
}

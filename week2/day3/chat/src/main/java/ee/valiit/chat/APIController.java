package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

@RestController
@CrossOrigin
public class APIController {

    @Autowired        // @Autowired - käsk mis ühendab ära jdbc klassi
            JdbcTemplate jdbcTemplate; // Jdbc - Dependency

    @GetMapping("/chat/{room}") // Get - gettüüpi päring. SELECT * FROM message
    ArrayList<ChatMessage> chat (@PathVariable String room){
        try { // error parandamiseks

        String sqlKask = "SELECT * FROM messages";
        ArrayList messages = (ArrayList)jdbcTemplate.query(sqlKask, (resultSet, rownum)-> {
            String username = resultSet.getString("username");
            String message = resultSet.getString("message");
            String url = resultSet.getString("url");
            return new ChatMessage(username, message, url);
        });
        return messages;
    }   catch(DataAccessException err){    // error parandamiseks. err - on muutuja nimi
            System.out.println("TABLE WAS NOT READY"); // error parandamiseks
            return new ArrayList<>();      // error parandamiseks
    }
    }

    @PostMapping("chat/{room}/new-message")
    void newMessage (@RequestBody ChatMessage msg, @PathVariable String room){ // ChatMessage - mis tüüpi, msg - message.
        String sqlKask = "INSERT INTO messages (username, message, url, room) VALUES ('" + msg.getUsername() + "', '" + msg.getMessage() + "', '" + msg.getUrl() + "', '" + room + "')";
        jdbcTemplate.execute(sqlKask);
    }
}

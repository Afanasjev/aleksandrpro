package ee.valiit.chat;
public class ChatMessage {
    private int id;
    private String username;
    private String room;
    private String message;
    private String url;

    public ChatMessage(){ // teeme tühja chatMessage
    }

    public ChatMessage(String username, String message, String url) {
        this.username = username;
        this.message = message;
        this.url = url;
    }

    // teeme konstruktori

    public String getUsername() {
        return username;
    }

    public String getRoom() {
        return room;
    }

    public String getMessage() {
        return message;
    }

    public int getId() {
        return id;
    }

    public String getUrl(){
        return url;
    }
}

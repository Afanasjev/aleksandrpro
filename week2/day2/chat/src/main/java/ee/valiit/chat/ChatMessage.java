package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChatMessage {

    private String user;
    private String message; // alt+enter - create getter for message

    private String url;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date date;

    public ChatMessage(){

    }

    public ChatMessage(String user, String message, String url) { //teeme konsrtuktori
        this.user = user;
        this.message = message;
        this.url = url;
        this.date = new Date();

    }

    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }


    public String getUrl() {
        return url;
    }

    public Date getDate() {
        return date;
    }
}

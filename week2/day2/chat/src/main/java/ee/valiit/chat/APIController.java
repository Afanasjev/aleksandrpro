package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class APIController {
    ChatRoom general = new ChatRoom("general"); // see on klassruum

    @GetMapping("/chat/general") // Get - gettüüpi päring
    ChatRoom chat1 (){
        return general;
    }

    @PostMapping("chat/general/new-message")
    void newMessage (@RequestBody ChatMessage msg){ // ChatMessage - mis tüüpi, msg - message.
        general.addMessage(msg); // võib ka - this.general.addMessage(msg);

    }
}

console.log("hommik!")

var sisend = 7
var tulemus

// kui on vähem kui 7, korruta kahega
// Kui on suurem kui 7, jaga kahega
// Kui ongi täpselt 7, jäta samaks


if (sisend == 7) {
	tulemus = sisend
} else if (sisend < 7) {
	tulemus = sisend * 2
} else {
	tulemus = sisend / 2
}

console.log("Tulemus on: " + tulemus)






/*
	Kui sõnad on võrdsed, siis prindi üks,
	kui erinevad, siis liida kokku ja prindi.
*/

var str1 = "banaan"
var str2 = "apelsin"

if (str1 == str2) {
	console.log(str1)
} else {
	console.log(str1 + " " + str2)
}







/*
	Meil on linnade nimekiri, aga ilma sõnata "linn". Need palun lisada.
*/

var linnad = ["Tallinn", "Tartu", "Valga"] // Defineerime nimekirja
var uuedLinnad = [] // Defineerime nimekirja kuhu tulemused panna

while (linnad.length > 0) { // Kuni linnasid on listus
	var linn = linnad.pop() // võta välja viimne
// pop - võtab " " tagant
	var uusLinn = linn + " linn" // ja lisa "linn" otsa
	uuedLinnad.push(uusLinn) // tulemus salvesta uude listi
// push - tõstab ette poole
}

console.log(uuedLinnad)





/*
	Eralda poiste ja tüdrukute nimed
*/

var nimed = ["Margarita", "Mara", "Mart", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []

while (nimed.length > 0) {


	// "a" lõpuga on tüdruku nimi. Googelda kuidas seda küsida.
	// Kui on poisi nimi, lisa see poisteNimed listi

	// Kui on tüdruku nimi, lisa see tydrukuteNimed listi
	var nimi = nimed.pop()
	if (nimi.endsWith("a")) {
		tydrukuteNimed.push(nimi)
	} else {
		poisteNimed.push(nimi)
	}
}

console.log(poisteNimed, tydrukuteNimed)

// ctrl + klikk - saad mitu kursorit
// ctrl + enter - tekitad uue rea
// consol.log("muutuja", muutuja) prindib






/* FUNKTSIOONID */

/* 
	Kirjuta algoritm, mis suudab ükskõik mis naise/mehe nime eristada.
*/

var eristaja = function(nimi) {
	if (nimi.endsWith("a")) {
		return "tüdruk"
	} else {
		return "poiss"
	}
}

var paeguneNimi = "Peeter"
var kumb = eristaja(paeguneNimi)
console.log(kumb)






/*
	Loo funktsioon, mis tagastab vastuse küsimusele, kas tegu on numbriga?
	!isNan(4) ehk is Not a number. Hüüumärk pöörab true/false vastupidi.
*/

var kasOnNumber = function(number) {
	if (!isNaN(number)) {
		return true
	} else {
		return false
}}

console.log( kasOnNumber(4) )
console.log( kasOnNumber("mingi sõne") )
console.log( kasOnNumber(2345) )
console.log( kasOnNumber(6.876) )
console.log( kasOnNumber(null) )
console.log( kasOnNumber([1, 4, 5, 6]) ) 



/* 
	Kirjuta funktsioon, mis võtab vastus kaks numbrit ja tagasta nende summa.
*/


var summa = function(a, b) {
		return a + b
	
}

console.log( summa(4, 8) )
console.log( summa("mingi sõne") )
console.log( summa(2345) )
console.log( summa(8, 85) )
console.log( summa(null) )
console.log( summa([1, 4, 5, 6]) )

/*
	
*/

var inimesed = {
	"Kaarel": 34, // võti: väärtus
	"Margarita": 10, // võti: väärtus
	"Suksu": [1, 4, 5, 6] , // võti: väärtus
	"Aleksandr": {
		vanus: 38,
		sugu: true
	}
}

console.log(inimesed["Kaarel"])
console.log(inimesed.Kaarel)
console.log(inimesed.Aleksandr.sugu)
console.log(inimesed.Suksu[1])